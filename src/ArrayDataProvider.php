<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-array library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataProvider;

use ArrayIterator;
use Iterator;

/**
 * ArrayDataProvider class file.
 * 
 * This data provider provides one object of arbitrary data under array form.
 * 
 * @author Anastaszor
 */
class ArrayDataProvider implements DataProviderInterface
{
	
	/**
	 * The source of this provider.
	 * 
	 * @var string
	 */
	protected string $_source;
	
	/**
	 * The data.
	 * 
	 * @var array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>>
	 */
	protected array $_data = [];
	
	/**
	 * Builds a new ArrayDataProvider with arbitrary data.
	 * 
	 * @param string $source
	 * @param array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>> $data
	 */
	public function __construct(string $source, array $data)
	{
		$this->_source = $source;
		$this->_data = $data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$types = [];
		
		foreach($this->_data as $key => $value)
		{
			$typeval = \gettype($value);
			$types[] = '"'.((string) $key).'" => '.$typeval;
		}
		
		return static::class.'@['.$this->_source.']{'.\implode(', ', $types).'}';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::getSource()
	 */
	public function getSource() : string
	{
		return $this->_source;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::hasUnique()
	 */
	public function hasUnique() : bool
	{
		return \count($this->_data) === 1;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideOne()
	 */
	public function provideOne() : array
	{
		if(\count($this->_data) === 0)
		{
			return [];
		}
		
		$value = \array_values($this->_data)[0];
		if(\is_array($value))
		{
			return $value;
		}
		
		return [$value];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideAll()
	 * @psalm-suppress InvalidReturnType
	 */
	public function provideAll() : array
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return \array_values($this->_data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideIterator()
	 * @psalm-suppress InvalidReturnType
	 */
	public function provideIterator() : Iterator
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return new ArrayIterator(\array_values($this->_data));
	}
	
}
