<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-array library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\ArrayDataProvider;
use PHPUnit\Framework\TestCase;

/**
 * ArrayDataProviderTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\ArrayDataProvider
 *
 * @internal
 *
 * @small
 */
class ArrayDataProviderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ArrayDataProvider
	 */
	protected ArrayDataProvider $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@['.__FILE__.']{"foo" => string, "qux" => string}', $this->_object->__toString());
	}
	
	public function testGetSource() : void
	{
		$this->assertEquals(__FILE__, $this->_object->getSource());
	}
	
	public function testHasUnique() : void
	{
		$this->assertFalse($this->_object->hasUnique());
	}
	
	public function testProvideOne() : void
	{
		$expected = [0 => 'bar'];
		
		$this->assertEquals($expected, $this->_object->provideOne());
	}
	
	public function testProvideAll() : void
	{
		$expected = [
			0 => 'bar',
			1 => 'baz',
		];
		
		$this->assertEquals($expected, $this->_object->provideAll());
	}
	
	public function testProvideIterator() : void
	{
		$expected = new ArrayIterator([
			0 => 'bar',
			1 => 'baz',
		]);
		
		$this->assertEquals($expected, $this->_object->provideIterator());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ArrayDataProvider(__FILE__, [
			'foo' => 'bar',
			'qux' => 'baz',
		]);
	}
	
}
