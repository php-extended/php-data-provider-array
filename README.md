# php-extended/php-data-provider-array

An implementation of php-extended/php-data-provider-interface that gets its data from php arrays.

![coverage](https://gitlab.com/php-extended/php-data-provider-array/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-data-provider-array/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-data-provider-array ^8`


## Basic Usage

You may use this library the following way :

```php

use PhpExtended\DataProvider\ArrayObjectProvider;

$provider = new ArrayObjectProvider(['foo' => 'bar']);

foreach($provider->provideIterator() as $line => $data)
{
	// do something with $data = ['foo' => 'bar']
}

```


## License

MIT (See [license file](LICENSE)).
